package com.dsf.example.play.actors

import java.nio.file.Path
import javax.inject.Inject

import akka.actor._
import com.dsf.example.play.models.pgsql.PostalCodeDAO

/**
  * Created by chaehb on 8/23/16.
  */
class DataInputActor @Inject()(postalCodeDAO: PostalCodeDAO) extends Actor {
  @scala.throws[Exception](classOf[Exception])
  override def preStart(): Unit = {
    super.preStart()

    println("start actor ... ")
  }

  @scala.throws[Exception](classOf[Exception])
  override def postStop(): Unit = {
    println("stop actor ....")

    super.postStop()
  }

  override def receive: Receive = {
    case path:String =>
    case _ =>
  }
}
